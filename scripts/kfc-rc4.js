const fs = require ('fs');
const rc4xor = require ('../lib/rc4xor');

// Read in the two ciphertexts and the plaintext as arrays of bytes.
const c1 = [ ...fs.readFileSync ('../../kishi/naga1/data/kim-space-speech.kfc') ];
const p1 = [ ...fs.readFileSync ('../../kishi/naga1/data/kim-space-speech.txt') ];
const c2 = [ ...fs.readFileSync ('../../kishi/naga1/data/secret-message.kfc') ];

console.log (rc4xor (c1) (c2) (p1));
// -> Our glorious leader wishes to inform you that the password for user naga1 is QFOWVtfPHP5BVTbcExvRSPwhOvhMX9nb
