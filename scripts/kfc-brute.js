const S = require('sanctuary');
const brute = require('../lib/brute').brute;
const IncrementingSequencer = require('../lib/sequencers/incrementing');
const shellExec = require('../lib/executors/shell');

const charset = S.map (String.fromCharCode) ([
    ...S.range (65) (91), // Uppercase
    ...S.range (97) (123), // Lowercase
    ...S.range (48) (58), // Digits
]);

brute 
    (IncrementingSequencer (charset)) 
    //(shellExec (key => `../kfc-encrypt ${key} < ../data/kim-space-speech.kfc`))
    (key => `../kfc-encrypt ${key} < ../data/kim-space-speech.kfc`)
    (S.pipe ([ console.log, () => false ]))
    ('Potato')
    (7);
