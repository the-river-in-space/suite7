const S = require ('sanctuary');

// rebase :: Number -> Number -> Number -> String
const rebase = fromBase => toBase => num => Number(parseInt (num, fromBase)).toString(toBase);

// decimal2hex :: Number -> String
const decimal2hex = rebase (10) (16);

// hex2decimal :: String -> Number
const hex2decimal = S.pipe ([ rebase (16) (10), Number ]);

// hexArray2decimalArray :: Array String -> Array Number
const hexArray2decimalArray = S.map (hex2decimal);

// text2asciiArray :: String -> Array Number
const text2asciiArray = S.pipe ([
    S.splitOn (''),
    S.map (char => char.charCodeAt (0)),
]);

// asciiArray2text :: Array Number -> String
const asciiArray2text = S.pipe ([
    S.map (String.fromCharCode),
    S.reduce (S.concat) (''),
]);

// xor :: Array Number -> Array Number -> Array Number
const xor = num1 => S.pipe ([
    S.zip (num1),
    S.map (zipped => S.fst (zipped) ^ S.snd (zipped)),
]);

module.exports = {
    rebase,
    decimal2hex,
    hex2decimal,
    hexArray2decimalArray,
    text2asciiArray,
    asciiArray2text,
    xor,
};
