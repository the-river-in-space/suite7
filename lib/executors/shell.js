const { exec } = require('child_process');

const shell = cmdBuilder => async str => await exec (cmdBuilder (str)).stdio;

module.exports = shell;
