const S = require('sanctuary');

// Launch a brute force attack.

// Recursive implementation. Only useful with small keyspaces due to lack of tail call optimisation.
const recursive = 
    sequencer => 
        executor => 
            validator => 
                key => 
                    stopAtLength => 
                        S.when 
                            (S.not) 
                            (() => 
                                brute (sequencer) (executor) (validator) (sequencer (key)) (stopAtLength)
                            )
                            (S.or 
                                (S.gte (stopAtLength) (key.length)) 
                                (validator (executor (key)))
                            );

// Iterative implementation. Default.
const iterative = sequencer => executor => validator => key => stopAtLength => {
    const exec = S.pipe ([ executor, validator ]);
    do {
        if (exec (key)) {
            return key;
        }

        key = sequencer (key);
    } while (key.length < stopAtLength);
};

module.exports = {
    brute: iterative,
    iterative,
    recursive,
};
