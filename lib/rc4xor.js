const S = require ('sanctuary');
const transform = require ('./transform');

// Given two ciphertexts encrypted with the same key and a plaintext of one of the ciphertexts, recover
// the other ciphertext. Takes the texts as arrays of bytes due to issues with Javascript's unicode support.
// rc4xor :: Array Number -> Array Number -> Array Number -> String
const rc4xor = 
    cipherbytes1 => 
        cipherbytes2 => 
            plainbytes => S.pipe ([
                transform.xor (cipherbytes1),
                transform.xor (plainbytes),
                transform.asciiArray2text,
            ])(cipherbytes2);

module.exports = rc4xor;
