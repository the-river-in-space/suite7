const S = require('sanctuary');

// Increment a value, rolling over at the base boundary.
// incrementValue :: Number -> Number -> Number
const incrementValue = base => value => (value + 1) % base;

// Test if incrementing a value will trigger a roll over.
// incrementWillCarry :: Number -> Array Number -> Boolean
const incrementWillCarry = base => S.pipe ([ S.last, S.fromMaybe (0), S.add (1), S.equals (base) ]);

// Recursively increment values.
// recurse :: Number -> Array Number -> Array Number
const recurse = 
    base => 
        register => 
            S.append 
                (incrementValue (base) (S.fromMaybe (0) (S.last (register)))) 
                (incrementRegister (base) (S.fromMaybe ([]) (S.init (register))));

// Increment the last value.
// increment :: Number -> Array Number -> Array Number
const increment = 
    base => 
        register => 
            S.append 
                (incrementValue (base) (S.fromMaybe (-1) (S.last (register)))) 
                (S.fromMaybe ([]) (S.init (register)));

// Return the next register in the sequence.
// incrementRegister :: Number -> Array Number -> Array Number
const incrementRegister = base => register =>
    S.ifElse (incrementWillCarry (base))
        (recurse (base))
        (increment (base))
    (register);

// Split a string into register values.
// stringToRegister :: Array String -> String -> Array Number
const stringToRegister = 
    charset => 
        str =>
            S.map (char => charset.indexOf (char)) (S.splitOn ('') (str));

// Convert register values to a string.
// registerToString :: Array String -> Array Number -> String
const registerToString = 
    charset => 
        register => 
            S.fromMaybe ('') (
                S.reduce (S.concat) 
                    (S.Just('')) 
                    (S.map (S.flip (S.at) (charset)) (register))
            );

// Higher Order Function that creates a sequencer out of an array of characters. The returned
// function takes a string and returns the next string in the incrementation.
// sequencer :: Array String -> String -> String
const sequencer = charset => S.pipe ([ 
    stringToRegister (charset), 
    incrementRegister (S.size (charset)),
    registerToString (charset),
]);

module.exports = sequencer;
