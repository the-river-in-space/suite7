# suite7

Data manipulation tools for use in hacking challenges.

# Genesis

This started with me doing the challenge http://overthewire.org/wargames/kishi/

The first level requires you to break the encryption on a text file to get the password for the next level. My first idea was to try and brute force the decryption key, so I wrote a script to generate sequences of strings. I used Node becuase the runtime was installed and it was convenient, and I also decided to write it in a pure functional style using Sancutary. Finally, I wanted something that could generate sequences of strings based on arbitrary character sets.

The incrementing sequencer turned out to be one of the most difficult things I've ever tried to program.

The brute force method turned out to be fruitless, and careful examination of the encryption algorithm lead to me figuring out which cipher was used. I then wrote a decryptor for that cipher, still in the functional style.

As I solved the challenge, work on these scripts stopped, but if I ever need more tools, I'll come back and do more work on this.
